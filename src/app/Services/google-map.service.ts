import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GoogleMapService {

  constructor(private http:HttpClient) { }

  getLocation(){
    return this.http.get('http')
  }

}
