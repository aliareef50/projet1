import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from '../Models/login';
import { Router } from '@angular/router';

const logintUrl = "http://topresa.ovh/api/login_check";

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(private httpClient : HttpClient, private router : Router) {}


  
  postLogin(login:Login):Observable<Login>{
    console.log(login);
    return this.httpClient.post<Login>(logintUrl,login);
  }

  logOut(){
     localStorage.removeItem('token');
     this.router.navigate(['home']);
      
  }
  getToken(){
    return localStorage.getItem('token');
  }
  loggedIn(){
    return !!this.getToken();
  }
  
}
