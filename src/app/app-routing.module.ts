import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { LoginComponent } from './Components/login/login.component';
import { ContactComponent } from './Components/contact/contact.component';
import { HotelsComponent } from './Components/hotels/hotels.component';
import { VoyageComponent } from './Components/voyage/voyage.component';


const routes: Routes = [
  {path :'hotels',component: HotelsComponent},
  {path :'contact',component: ContactComponent},
  {path :'home',component: HomeComponent},
  {path :'login',component:LoginComponent},
  {path :'voyage',component:VoyageComponent},
  /*{path :'client',component:ClientComponent,canActivate: [AuthGuard]},*/
  /*{path :'billets',component:VenteBilletsComponent},*/
  {path :'**',component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
